let Git = require('nodegit');
let gui = require('gui');

let getMostRecentCommit = (repository) => {
    return repository.getBranchCommit("master");
};

let getCommitMessage =  (commit) => {
    return commit.message();
};

// Git.Repository.open('~/Documents/off_projects/react-redux-training')
// .then(getMostRecentCommit)
// .then(getCommitMessage)
// .then(function (message) {
//     console.log(message);
// });


const win = gui.Window.create({})
win.setContentSize({ width: 400, height: 400 })
win.onClose = () => gui.MessageLoop.quit()

const contentView = gui.Container.create()
contentView.setStyle({ flexDirection: 'row' })
win.setContentView(contentView);


let sidebar;

sidebar = gui.Container.create();
sidebar.setStyle({ padding: 5 });
contentView.addChildView(sidebar);

const open = gui.Button.create('Open Folder');
sidebar.addChildView(open);

const messageLabel = gui.Label.create('Hello');
contentView.addChildView(messageLabel);

open.onClick = () => {
    const folderDialog = gui.FileOpenDialog.create();
    folderDialog.setOptions(gui.FileDialog.optionPickFolders);
    if (folderDialog.runForWindow(win)) {
        console.log(folderDialog.getResult());
        let pathToRepo = require("path").resolve(folderDialog.getResult());
        console.log(pathToRepo);
        messageLabel.setText('Kool');
        Git.Repository.open(pathToRepo)
        .then(getMostRecentCommit)
        .then(getCommitMessage)
        .then( (message) => {
            console.log(message);
            messageLabel.setText(message);
        });
        // Git.Repository.open(folderDialog.getResult()+'/')
        //     .then(getMostRecentCommit)
        //     .then(getCommitMessage)
        //     .then(function (message) {
        //         console.log(message);
        //     });


    }
}

win.center()
win.activate()

if (!process.versions.yode) {
    gui.MessageLoop.run()
    process.exit(0)
}