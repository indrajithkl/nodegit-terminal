let Git = require('nodegit');
let walker, repo;
var getMostRecentCommit = function (repository) {
    // console.log(repository.getBranchCommit("parcel-bundling").sha());
    return repository.getBranchCommit("parcel-bundling");
};

var getCommitMessage = function (commit) {
    return commit.message();
};

// Git.Repository.open('/home/inapp/Documents/off_projects/portfolio')
// .then(getMostRecentCommit)
// .then(getCommitMessage)
// .then(function (message) {
//     console.log(message);
// });


Git.Repository.open('/home/inapp/Documents/off_projects/portfolio')
.then((repository) => {
    repo = repository;
    return repository.getBranchCommit('parcel-bundling');
},(error) => {
    console.log('Error Getting Repository Details ',error);
}).then((commit) => {
    walker = repo.createRevWalk();
    walker.push(commit.sha());
    walker.sorting(Git.Revwalk.SORT.Time);
    console.log(commit.sha());

    return walker.getCommits(500);
}).then((commitArray) => {
    commitArray.forEach((commit) => {
        console.log(commit.message());
    })
})
