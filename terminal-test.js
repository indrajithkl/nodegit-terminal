var blessed = require('blessed');
let Git = require('nodegit');
let contrib = require('blessed-contrib');
var fs = require('fs');

let walker, repo;
// Create a screen object.
var screen = blessed.screen({
    smartCSR: true
});

screen.title = 'Git GUI';

// Create a box perfectly centered horizontally and vertically.
var box = blessed.box({
    top: 'center',
    left: 'center',
    width: '100%',
    height: '100%',
    content: 'GIT {bold}GUI{/bold}!',
    tags: true,
    border: {
        type: 'line'
    },
    style: {
        fg: 'white',
        bg: 'black',
        border: {
            fg: '#f0f0f0'
        },
        hover: {
            bg: 'black'
        }
    }
});



var getCommitsButton = blessed.button({
    parent: box,
    mouse: true,
    keys: true,
    shrink: true,
    padding: {
        left: 1,
        right: 1
    },
    left: 10,
    top: 2,
    shrink: true,
    name: 'submit',
    content: 'submit',
    style: {

    }
});

getCommitsButton.on('click', function (data) {
    getGitCommit();
    screen.render();
});


// Append our box to the screen.
screen.append(box);

// Add a png icon to the box
var icon = blessed.image({
    parent: box,
    top: 15,
    left: 12,
    type: 'ansi',
    width: '100%',
    height: '100%',
    file: __dirname + '/icons/save.png',
    search: false
});


var list = blessed.list({
    parent: screen,
    label: ' {bold}{cyan-fg}Commits{/cyan-fg}{/bold} ',
    tags: true,
    draggable: true,
    top: '25%',
    right: 0,
    width: '50%',
    height: '50%',
    keys: true,
    vi: true,
    mouse: true,
    border: 'line',
    scrollbar: {
        ch: ' ',
        track: {
            bg: 'cyan'
        },
        style: {
            inverse: true
        }
    },
    style: {
        item: {
            hover: {
                bg: 'blue'
            }
        },
        selected: {
            bg: 'blue',
            bold: true
        }
    }
});

// var table = blessed.listtable({
//     rows: [['Name', 'Test']],

//     parent: screen,
//     label: ' {bold}{cyan-fg}Commits with Author{/cyan-fg}{/bold} ',
//     tags: true,
//     draggable: true,
//     top: '10%',
//     right: 0,
//     width: '50%',
//     height: '50%',
//     keys: true,
//     vi: true,
//     mouse: true,
//     border: 'line',
//     scrollbar: {
//         ch: ' ',
//         track: {
//             bg: 'cyan'
//         },
//         style: {
//             inverse: true
//         }
//     },
//     style: {
//         item: {
//             hover: {
//                 bg: 'blue'
//             }
//         },
//         selected: {
//             bg: 'blue',
//             bold: true
//         },
//         cell: {
//             width: '25%',
//         }
//     }
// });
let loader = blessed.loading({
    parent: screen,
    top: 'center',
    left: 'center',
    height: 5,
    align: 'center',
    width: '50%',
    tags: true,
    hidden: true,
    border: 'line'
  });

var table = contrib.table(
    {
        parent: box,
        keys: true,
        fg: 'white',
        selectedFg: 'white',
        selectedBg: 'blue',
        interactive: true,
        draggable: true,
        label: 'Active Processes',
        top: '15%',
        width: '50%',
        height: '30%',
        border: {
            type: "line",
            fg: "cyan"
        },
        columnSpacing: 10,
        columnWidth: [16, 100]
    })

//allow control the table with the keyboard
table.focus()


// Quit on Escape, q, or Control-C.
screen.key(['escape', 'q', 'C-c'], function (ch, key) {
    return process.exit(0);
});


screen.render();


var getMostRecentCommit = function (repository) {
    return repository.getBranchCommit("master");
};

var getCommitMessage = function (commit) {
    return commit.message();
};

let getGitCommit = () => {
    // Git.Repository.open('/home/inapp/Documents/off_projects/portfolio')
    loader.load('Loading....')
    Git.Repository.open('/home/inapp/Documents/off_projects/angular')
        .then((repository) => {
            repo = repository;
            return repository.getBranchCommit('master');
        }, (error) => {
            console.log('Error Getting Repository Details ', error);
        }).then((commit) => {
            walker = repo.createRevWalk();
            walker.push(commit.sha());
            walker.sorting(Git.Revwalk.SORT.Time);
            // console.log(commit.sha());

            return walker.getCommits(500);
        }).then((commitArray) => {
            let messages = [];
            let messageTable = [];
            commitArray.forEach((commit) => {
                // console.log(commit.message());
                let commitArray = []
                messages.push(commit.message().replace(/['"]+/g, ''));
                commitArray.push(commit.author().toString());
                commitArray.push(commit.message().replace(/['"]+/g, ''));
                messageTable.push(commitArray);
            });

            fs.writeFile("./log", messages, function(err) {
                if(err) {
                    return console.log(err);
                }
            
                console.log("The file was saved!");
            }); 
            list.setItems(messages);
            // table.setData(messageTable);
            table.setData({
                    headers: ['Author', 'Message'],
                    data: messageTable
                });

                loader.stop();
            console.log(messageTable)
        })

}